package dxc;

public class RegexAssign2 {
	
	public static boolean checkWebAddressValidity(String webAddress) 
	{
		if(webAddress.matches("(http://|https://)(www\\.).*[a-zA-Z0-9]*(.com|.org|.net)"))
		{
			return true;
		}
		else 
			return false;
		
	}

	public static void main(String[] args) 
	{
		String webAddress = "http://www.company.com";
		 System.out.println("The web address is "+ (checkWebAddressValidity(webAddress) ? "valid!" : 
		"invalid!"));

	}

}
