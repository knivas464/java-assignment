package dxc;

class TV
{
	int channel=1,volumeLevel=1;
	boolean on=false;
	void setChannel(int newChannel)
	{
		if(on==true && newChannel>=1 && newChannel<=120 )
		{
		channel=newChannel;	
		}
	}
	void setVolumelevel(int newVolumelevel)
	{
		if(on==true && newVolumelevel>=1 && newVolumelevel<=7 )
		{
			volumeLevel=newVolumelevel;	
		}
	}
	void turnOn()
	{
		on=true;
	}
	void turnOff()
	{
		on=false;
	}
	void channelUp()
	{
		if(on==true && channel<120)
		{
			channel++;
		}
	}
	void channelDown()
	{
		if(on==true && channel>1)
		{
			channel--;
		}
	}
	void volumeUp()
	{
		if(on==true && volumeLevel<7)
		{
			volumeLevel++;
		}
	}
	void volumeDown()
	{
		if(on==true && volumeLevel>1)
		{
			volumeLevel--;
		}
	}
	void print()
	{
		System.out.println("Channel:"+channel+" volume :"+volumeLevel);
	}
}

public class Telivison {

	public static void main(String[] args) 
	{
		TV t1=new TV();
		t1.turnOn();
		t1.setChannel(5);
		t1.setVolumelevel(2);
		t1.channelUp();
		//watch.channeldown();
		t1.volumeUp();
		//watch.volumedown();
		t1.print();
	}

}
