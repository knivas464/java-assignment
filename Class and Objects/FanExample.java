package dxc;

class Fan
{
	private int SLOW;
	private int MEDIUM;
	private int FAST;
	private int speed;
	private boolean on;
	private double radius;
	private String color;
	
	Fan()
	{
		this.SLOW=1;
		this.MEDIUM=1;
		this.FAST=1;
		this.speed=SLOW;
		this.on=false;
		this.radius=5;
		this.color="blue";
	}
	
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public boolean isOn() {
		return on;
	}
	public void setOn(boolean on) {
		this.on = on;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public String toString() {
		return "Fan [speed=" + speed + ", on=" + on + ", radius=" + radius + ", color=" + color + "]";
	}
	
	
}

public class FanExample {

	public static void main(String[] args) 
	{
		Fan f1=new Fan();
		f1.setSpeed(5);
		f1.setOn(true);
		f1.setRadius(13);
		f1.setColor("Orange");
		System.out.println(f1.toString()); 	}

}
