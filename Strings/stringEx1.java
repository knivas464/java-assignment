package dxc;
import java.util.*;
public class stringEx1 {
	static String removeWhiteSpaces(String s)
	{
		s=s.replaceAll(" ", "");
		return s;
	}
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a sentence : ");
		String s=sc.nextLine();
		System.out.println(removeWhiteSpaces(s));
	}

}
