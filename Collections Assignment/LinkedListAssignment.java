package CollectionsAssignment;
import java.util.*;

public class LinkedListAssignment {
	
	public static List<Integer> removeDuplicates(List<Integer> list){
		List<Integer> sample=new LinkedList<Integer>();
		
		for(Integer i : list) {
			if(!sample.contains(i))
			{
				sample.add(i);
			}
		}
		return sample;
		
	}

	public static void main(String[] args) {
		
		List<Integer> list1=new LinkedList<Integer>();
		list1.add(10);
		list1.add(15);
		list1.add(21);
		list1.add(15);
		list1.add(10);
		
		List<Integer> list2=new LinkedList<Integer>();
		list2.add(51);
		list2.add(45);
		list2.add(45);
		list2.add(15);
		list2.add(82);
		list2.add(51);
		list2.add(10);
		
		System.out.println(removeDuplicates(list1));
		System.out.println(removeDuplicates(list2));
	}

}
