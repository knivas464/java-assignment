package CollectionsAssignment;
import java.util.*;

public class HashMapAssignment1 {
	
	public static List<String> sortSales(Map<String, Integer> sales) {
        List<Map.Entry<String, Integer>> list = new ArrayList<>(sales.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        List<String> result = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : list) {
            result.add(entry.getKey());
        }
        return result;
    }
	

	public static void main(String[] args) {
		        Map<String, Integer> set1 = new HashMap<>();
		        set1.put("Mathew", 50);
		        set1.put("Lisa", 76);
		        set1.put("Courtney", 45);
		        set1.put("David", 49);
		        set1.put("Paul", 49);
		        List<String> result1 = HashMapAssignment1.sortSales(set1);
		        System.out.println(result1);
		        
		        Map<String, Integer> set2 = new HashMap<>();
		        set2 .put("Mathew", 40);
		        set2 .put("Lisa", 60);
		        set2 .put("Courtney", 35);
		        set2 .put("David", 52);
		        set2 .put("Paul", 58);
		        List<String> result2 = HashMapAssignment1.sortSales(set2 );
		        System.out.println(result2); 
		    
	}

}
