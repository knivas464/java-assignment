package CollectionsAssignment;
import java.util.*;

public class QueueInterfaceAssigmentTester {
	
	public static List<Deque<Patient>> splitQueue(Deque<Patient> pQueue) {
		List<Deque<Patient>> finalList = new ArrayList<Deque<Patient>>();
		Deque<Patient> agedQueue = new ArrayDeque<Patient>();
		Deque<Patient> normalQueue = new ArrayDeque<Patient>();
		
		for (Patient patient : pQueue) {
			if(patient.getAge() >= 60) {
				agedQueue.add(patient);
			}
			else {
				normalQueue.add(patient);
			}
		}
		
		finalList.add(agedQueue);
		finalList.add(normalQueue);
		
		return finalList;
	}

	public static void main(String[] args) {
		
		Patient patient1 = new Patient("Jack","Male",25);
		Patient patient2 = new Patient("Tom","Male",64);
		Patient patient3 = new Patient("Simona","Female",24);
		
		Deque<Patient> patientsQueue = new ArrayDeque<Patient>();
		patientsQueue.add(patient1);
		patientsQueue.add(patient2);
		patientsQueue.add(patient3);
		
		List<Deque<Patient>> queuesList = splitQueue(patientsQueue);
	
		int counter=0;
		for (Deque<Patient> queue : queuesList) {
			if(counter==0)
				System.out.println("Patients in the senior queue\n============================");
			else
				System.out.println("Patients in the normal queue\n============================");
			
			for (Patient patient : queue) {
				System.out.println("Name : "+patient.getName());
				System.out.println("Age : "+patient.getAge());
				System.out.println();
			}
			counter++;
			
	}
		

	}

}
