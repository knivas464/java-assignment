package CollectionsAssignment;
import java.util.*;

public class LinkedListAssignment2 {
	
	public static List<Integer> findCommonElements(List<Integer> list1,List<Integer> list2){
		
		List<Integer> CommonElements= new LinkedList<Integer>();
		
		for(Integer i:list1) {
			if(list2.contains(i)) {
				CommonElements.add(i);
			}
		}return CommonElements;
	}

	public static void main(String[] args) 
	{
		List<Integer> list1=new LinkedList<Integer>();
		list1.add(10);
		list1.add(12);
		list1.add(21);
		list1.add(1);
		list1.add(53);
		
		List<Integer> list2=new LinkedList<Integer>();
		list2.add(11);
		list2.add(21);
		list2.add(25);
		list2.add(53);
		list2.add(47);
		
		List<Integer> list3=new LinkedList<Integer>();
		list3.add(51);
		list3.add(45);
		list3.add(63);
		list3.add(15);
		list3.add(82);
		
		List<Integer> list4=new LinkedList<Integer>();
		list4.add(19);
		list4.add(63);
		list4.add(51);
		list4.add(87);
		list4.add(82);
		
		System.out.println(findCommonElements(list1,list2));
		System.out.println(findCommonElements(list3,list4));
	}

}
