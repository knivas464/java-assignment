package CollectionsAssignment;
import java.util.*;

public class HashMapAssignment2 {
	 public static void main(String[] args) {
	        HashMap<String, Integer> H1 = new HashMap<>();
	        H1.put("Kelly", 10);
	        H1.put("Micheal", 20);
	        H1.put("Ryan", 30);

	        HashMap<String, Integer> H2 = new HashMap<>();
	        H2.put("Jim", 15);
	        H2.put("Andy", 45);

	        HashMap<String, Integer> firstMergedMap= mergeMaps(H1, H2);
	        System.out.println(firstMergedMap); 
	        
	        HashMap<String, Integer> H3 = new HashMap<>();
	        H3.put("Toby", 15);
	        H3.put("Micheal", 20);
	        H3.put("Angela", -30);

	        HashMap<String, Integer> H4 = new HashMap<>();
	        H4.put("Toby", 15);
	        H4.put("Andy", 45);
	        H4.put("Micheal", 40);

	        HashMap<String, Integer> SecondMergedMap = mergeMaps(H3, H4);
	        System.out.println(SecondMergedMap); 
	    }

	    public static HashMap<String, Integer> mergeMaps(HashMap<String, Integer> m1 ,HashMap<String, Integer> m2) {
	        HashMap<String, Integer> mergedMap = new HashMap<>();
	        for (String key : m1.keySet()) {
	            if (m2.containsKey(key)) {
	                mergedMap.put(key + "new", m2.get(key));
	                m2.remove(key);
	            } else {
	                mergedMap.put(key, m1.get(key));
	            }
	        }
	        mergedMap.putAll(m2);
	        return mergedMap;
	    }

}
