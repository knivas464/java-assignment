package CollectionsAssignment;
import java.util.*;

class Participant
{
	public String Name;
	public String Talent;
	public double marks;
	
	public Participant(String Name,String Talent,double marks)
	{
		this.Name=Name;
		this.Talent=Talent;
		this.marks=marks;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getTalent() {
		return Talent;
	}

	public void setTalent(String talent) {
		Talent = talent;
	}

	public double getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Participant (\"" + Name + "\",\"" + Talent + "\"," + marks + ")";
	}
	
}

public class ArrayListAssignment {

	public static void main(String[] args) {
		
		List<Participant> finalists1=new ArrayList<Participant>();
		finalists1.add(new Participant("Hazel","Singing",91.2));
		finalists1.add(new Participant("Ben","Instrumental",95.7));
		finalists1.add(new Participant("John","Singing",94.5));
		finalists1.add(new Participant("Bravo","Singing",97.6));
		String talent1="Singing";
		
		List<Participant> finalists2=new ArrayList<Participant>();
		finalists2.add(new Participant("Mark","Instrumental",81.2));
		finalists2.add(new Participant("Ella","Instrumental",65.7));
		finalists2.add(new Participant("Lily","Singing",86.5));
		String talent2="Singing";
		
		System.out.println(generateListOfFinalists(finalists1));
		System.out.println(getFinalistsByTalents(finalists1,talent1));
		System.out.println("\n");
		System.out.println(generateListOfFinalists(finalists2));
		System.out.println(getFinalistsByTalents(finalists2,talent2));
		
		
	}

	private static List<Participant> generateListOfFinalists(List<Participant> finalists) {
		
		return finalists;
	}
	
	private static List<Participant> getFinalistsByTalents(List<Participant> finalists,String talent){
		List<Participant> finalistsByTalents=new ArrayList<Participant>();
		for(Participant p: finalists) {
			if(p.getTalent()==talent) {
				finalistsByTalents.add(p);
			}
		}
		return finalistsByTalents;
	}

}
