package CollectionsAssignment;
import java.util.*;

public class QueueInterfaceAssignment2 {
	
	public static Deque<Character> updateStack(Deque<Character> stack) {
		Deque<Character> tempStack = new ArrayDeque<Character>();
		for (int i = 0; i < 3; i++) {
			tempStack.push(stack.removeLast()); 
		}
		Deque<Character> revStack = new ArrayDeque<Character>();
		for(Character c:tempStack) {
			revStack.push(c);
		}
		
		while (!revStack.isEmpty()) {
			stack.push(revStack.pop()); // pop from tempStack and push to inputStack
		}
		return stack;
	}
	
	public static void main(String[] args) {
		
		Deque<Character> stack1 = new ArrayDeque<Character>();
		stack1.push('E');
		stack1.push('D');
		stack1.push('C');
		stack1.push('B');
		stack1.push('A');
		
		Deque<Character> resultStack = updateStack(stack1);
		
		System.out.println("The result stack alphabets are :");
		for(Character a: resultStack)
		    System.out.println(a);
	}

}
