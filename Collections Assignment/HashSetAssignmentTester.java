package CollectionsAssignment;
import java.util.*;

public class HashSetAssignmentTester {
	
	public static Set<Student> findUnique(List<Student> student){
		Set<Student> uniqueStudents=new HashSet<Student>();
		for(Student s:student) {
			if(!uniqueStudents.contains(s)) {
				uniqueStudents.add(s);
			}
		}return uniqueStudents;
	}
	
	public static Set<Student> findDuplicate(List<Student> student){
		Set<Student> duplicateStudents=new HashSet<Student>();
		Set<String> UniqueStudents=new HashSet<String>();
		for(Student s:student) {
			if(!UniqueStudents.contains(s.getEmailId())) {
				UniqueStudents.add(s.getEmailId());
			}
			else
				duplicateStudents.add(s);
		}return duplicateStudents;
	}

	public static void main(String[] args) {
		
		List<Student> students = new ArrayList<Student>();

        students.add(new Student(5004, "Wyatt", "Wyatt@example.com", "Dance"));
        students.add(new Student(5010, "Lucy", "Lucy@example.com", "Dance"));
        students.add(new Student(5550, "Aaron", "Aaron@example.com", "Dance"));
        students.add(new Student(5560, "Ruby", "Ruby@example.com", "Dance"));
        students.add(new Student(5015, "Sophie", "Sophie@example.com", "Music"));
        students.add(new Student(5013, "Clara", "Clara@example.com", "Music"));
        students.add(new Student(5010, "Lucy", "Lucy@example.com", "Dance"));
        students.add(new Student(5011, "Ivan", "Ivan@example.com", "Music"));
        students.add(new Student(5550, "Aaron", "Aaron@example.com", "Dance"));
        
        System.out.println("Unique students:");
        for(Student s:findUnique(students)) {
        	System.out.println(s);
        }
        System.out.println("Duplicate students:");
        for(Student s:findDuplicate(students)) {
        	System.out.println(s);
        }
	}

}
