package dxc;
import java.util.*;
public class HomeDelivery {

	public static void main(String[] args) 
	{
		int del_fee;
		int total=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter V for Veg and N for Non-veg");
		char c=sc.next().charAt(0);
		System.out.println("Enter the quantitiy ordered : ");
		int q=sc.nextInt();
		System.out.println("Enter the distance in Kms : ");
		int d=sc.nextInt();
		if(c=='V' || c=='v')
		{
			if(d>0 && d<=3)
			{
				del_fee=0;
				total=del_fee+q*12;
			}
			else if(d>3 && d<=6)
			{
				del_fee=1;
				total=q*12+del_fee;
			}
			else if(d>6)
			{
				del_fee=2;
				total=q*12+(d-5)*2+1;
			}
			else
			{
				System.out.println("Enter valid input");
			}
		}
		else if(c=='N' || c=='n')
		{
			if(d>0 && d<=3)
			{
				del_fee=0;
				total=del_fee+q*15;
			}
			else if(d>3 && d<=6)
			{
				del_fee=1;
				total=q*15+del_fee;
			}
			else if(d>6)
			{
				del_fee=2;
				total=q*15+(d-5)*2+1;
			}
			else
			{
				System.out.println("Enter valid input");
			}
		}
		else
		{
			System.out.println("Enter valid input");
		}
		System.out.println("Total amount is : "+total );
	}

}
