package dxc;
import java.util.*;

public class luckyNumber {

	public static void main(String[] args) 
	{
		//finding reverse of the number
		int count=0;
		int rev=0;
		int org,d,sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number : ");
		int num=sc.nextInt();
		org=num;
		while(num>0)
		{
			num=num/10;
			count++;
		}
		int org1=org;
		while(org>0)
		{
			d=org%10;
			rev=rev*10+d;
			org=org/10;
		}
		
		// finding if lucky or not
		int position=1,even_position;
		while(rev>0)
		{
			if(position%2==0)
			{
				even_position=rev%10;
				sum=sum+even_position*even_position;
				
			}	
			rev=rev/10;
			position++;
		}
		if(sum%9==0)
		{
			System.out.println("The number "+org1+" is a Lucky number");
		}
		else
		{
			System.out.println("The number "+org1+" is not a Lucky number");
		}
			
	}

}
